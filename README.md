# Coding Test for Revolut #

I implemented this task using the new Android Architecture components (MVVM).

However, I ran out of time so there are some areas which could be improved, notibly:

* Error handling (should switch to using a Response)
* Testing
* Saving the transaction back to the account
* There is probably a better solution than using a PagerAdapter
* Using JavaMoney
* UI is rough