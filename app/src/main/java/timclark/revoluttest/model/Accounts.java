package timclark.revoluttest.model;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

@Singleton
public class Accounts {

    private List<Account> accounts = new ArrayList<>();

    public List<Account> getAccounts() {
        return accounts;
    }

    public void add(Account account) {
        accounts.add(account);
    }

}
