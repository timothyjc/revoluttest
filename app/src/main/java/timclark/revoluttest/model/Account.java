package timclark.revoluttest.model;

// TODO: use JavaMoney or JodaMoney
public class Account {

    private String currencyCode;
    private double amount;

    public Account(String currencyCode, float amount) {
        this.currencyCode = currencyCode;
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return currencyCode != null ? currencyCode.equals(account.currencyCode) : account.currencyCode == null;
    }

    @Override
    public int hashCode() {
        return currencyCode != null ? currencyCode.hashCode() : 0;
    }

}
