package timclark.revoluttest.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatesContainer {

    @SerializedName("base") @Expose private String base;
    @SerializedName("date") @Expose private String date;
    @SerializedName("rates") @Expose private Rates rates;
    private long creationDate = 0;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    @Nullable
    public Double getRate(String currencyCode) {
        switch (currencyCode) {
            case "EUR":
                return rates.getRateEUR();
            case "GBP":
                return rates.getRateGBP();
            case "USD":
                return rates.getRateUSD();
        }
        return null;
    }

}
