package timclark.revoluttest.repository;

import java.util.HashMap;
import java.util.Map;

import timclark.revoluttest.model.RatesContainer;

public class RatesCache {

    private static final long CACHE_EXPIRY_TIME = 30000; // ms

    private Map<String, RatesContainer> cache = new HashMap<>();

    public void put(RatesContainer ratesContainer) {
        cache.put(ratesContainer.getBase(), ratesContainer);
    }

    public RatesContainer get(String base) {
        RatesContainer ratesContainer = cache.get(base);
        if (ratesContainer != null) {
            long created = ratesContainer.getCreationDate();
            long delta = System.currentTimeMillis() - created;
            if (delta < CACHE_EXPIRY_TIME) {
                return ratesContainer;
            }
        }
        return null;
    }


}
