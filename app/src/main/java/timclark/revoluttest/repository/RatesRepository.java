package timclark.revoluttest.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;
import timclark.revoluttest.RevolutApplication;
import timclark.revoluttest.api.FixerService.FixerApi;
import timclark.revoluttest.model.RatesContainer;

@Singleton
public class RatesRepository {

    @Inject FixerApi fixerApi;
    @Inject RatesCache ratesCache;
    private final MutableLiveData<RatesContainer> rates = new MutableLiveData<>();

    public RatesRepository() {
        RevolutApplication.getAppComponent().inject(this);
    }

    public LiveData<RatesContainer> getRates() {
        return rates;
    }

    public void refreshRates(@NonNull String currencyCode) {
        RatesContainer ratesContainer = ratesCache.get(currencyCode);
        if (ratesContainer != null) {
            rates.postValue(ratesContainer);
        } else {
            Timber.d("Querying API for new rates: " + currencyCode);
            fixerApi.listRates(currencyCode).enqueue(new Callback<RatesContainer>() {
                @Override
                public void onResponse(@NonNull Call<RatesContainer> call, @NonNull Response<RatesContainer> response) {
                    RatesContainer ratesContainer = response.body();
                    if (ratesContainer != null) {
                        ratesContainer.setCreationDate(System.currentTimeMillis());
                        ratesCache.put(ratesContainer);
                        rates.setValue(ratesContainer);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RatesContainer> call, @NonNull Throwable t) {
                    Timber.e("Unable to get rates " + t.getMessage(), t);
                    // TODO: Should return a Resource for error handling.
                }
            });
        }
    }

}
