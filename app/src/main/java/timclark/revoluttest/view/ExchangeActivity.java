package timclark.revoluttest.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antonyt.infiniteviewpager.InfinitePagerAdapter;
import com.antonyt.infiniteviewpager.InfiniteViewPager;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import me.relex.circleindicator.CircleIndicator;
import timber.log.Timber;
import timclark.revoluttest.R;
import timclark.revoluttest.RevolutApplication;
import timclark.revoluttest.model.Account;
import timclark.revoluttest.model.Accounts;
import timclark.revoluttest.model.RatesContainer;
import timclark.revoluttest.util.StringUtils;
import timclark.revoluttest.view.adapters.CurrencyPagerAdapter;
import timclark.revoluttest.view.adapters.CurrencyPagerAdapter.InputChangedListener;
import timclark.revoluttest.viewmodel.ExchangeViewModel;

public class ExchangeActivity extends AppCompatActivity {

    private static final int POLLING_RATE_SECONDS = 30;

    @Inject Accounts accounts;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.from_view_pager) InfiniteViewPager fromPager;
    @BindView(R.id.from_indicator) CircleIndicator fromIndicator;
    @BindView(R.id.to_view_pager) InfiniteViewPager toPager;
    @BindView(R.id.to_indicator) CircleIndicator toIndicator;
    @BindView(R.id.conversion_rate) TextView conversionRateTextView;
    @BindView(R.id.tick) ImageView tickButton;

    private ExchangeViewModel exchangeViewModel;
    private Account fromAccount;
    private Account toAccount;

    private enum Mode {TOP_TO_BOTTOM, BOTTOM_TO_TOP}

    private Mode mode = Mode.TOP_TO_BOTTOM;

    private CurrencyPagerAdapter fromAdapter;
    private CurrencyPagerAdapter toAdapter;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        RevolutApplication.getAppComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(null);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        exchangeViewModel = ViewModelProviders.of(this).get(ExchangeViewModel.class);
        exchangeViewModel.getRates().observe(this, ratesContainer -> {
            Timber.d("Got new rates!");
            updateHeaderRate();
            validate();
            calculateExchange();
        });

        initFromViewPager();
        initToViewPager();

        compositeDisposable.add(
                Observable.interval(POLLING_RATE_SECONDS, TimeUnit.SECONDS, Schedulers.io())
                        .startWith(0L)
                        .subscribeOn(Schedulers.io())
                        .map(tick -> {
                            exchangeViewModel.refreshRates(fromAccount.getCurrencyCode());
                            return true;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe()
        );

        tickButton.setOnClickListener(view -> processExchange());

        validate();

    }

    private void processExchange() {
        // TODO: make transfer
        Toast.makeText(this, "TODO: make the transfer", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        fromAdapter.onDestroy();
        toAdapter.onDestroy();
    }

    private void initFromViewPager() {
        fromAdapter = new CurrencyPagerAdapter(new InputChangedListener() {
            @Override
            public void onInputChanged() {
                onFromValueChanged();
            }

            @Override
            public void onFocus() {
                mode = Mode.TOP_TO_BOTTOM;
                calculateExchange();
            }
        }, true);
        fromPager.setAdapter(new InfinitePagerAdapter(fromAdapter));
        fromIndicator.setViewPager(fromPager);
        fromPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                int pos = position % accounts.getAccounts().size(); // for infinite pager
                fromAccount = accounts.getAccounts().get(pos);
                clearAmountAndRate(fromAdapter, fromPager, fromAccount.getCurrencyCode());
                updateHeaderRate();
                validate();
                exchangeViewModel.refreshRates(fromAccount.getCurrencyCode());
            }
        });
        fromAccount = accounts.getAccounts().get(0);
        fromPager.setCurrentItem(0);
    }

    private void clearAmountAndRate(CurrencyPagerAdapter adapter, View container, String currencyCode) {
        runOnUiThread(() -> adapter.updatePanel(container, currencyCode, 0, ""));
    }

    private void onFromValueChanged() {
        calculateExchange();
    }

    private void calculateExchange() {

        String fromCurrencyCode = fromAccount.getCurrencyCode();
        String toCurrencyCode = toAccount.getCurrencyCode();

        Double rate = getRate();
        if (rate == null) {
            switch (mode) {
                case TOP_TO_BOTTOM:
                    runOnUiThread(() -> toAdapter.updatePanel(toPager, toCurrencyCode, 0, ""));
                    break;
                case BOTTOM_TO_TOP:
                    runOnUiThread(() -> fromAdapter.updatePanel(fromPager, fromCurrencyCode, 0, ""));
                    break;
            }
        } else {
            double reverseRate = 1 / rate;
            switch (mode) {
                case TOP_TO_BOTTOM: {
                    // need to display the bottom rate and amount
                    double toAmount = getFromInput() * rate;
                    String rateString = StringUtils.getRateString(this, toCurrencyCode, fromCurrencyCode, reverseRate, "");
                    runOnUiThread(() -> toAdapter.updatePanel(toPager, toCurrencyCode, toAmount, rateString));
                    break;
                }
                case BOTTOM_TO_TOP: {
                    // need to display the top rate and amount
                    double fromAmount = getToInput() * reverseRate;
                    String rateString = StringUtils.getRateString(this, fromCurrencyCode, toCurrencyCode, rate, "");
                    runOnUiThread(() -> fromAdapter.updatePanel(fromPager, fromCurrencyCode, fromAmount, rateString));
                    break;
                }
            }
        }

        validate();
    }

    @Nullable
    private Double getRate() {
        String fromCurrencyCode = fromAccount.getCurrencyCode();
        String toCurrencyCode = toAccount.getCurrencyCode();
        RatesContainer ratesContainer = exchangeViewModel.getRates().getValue();
        if (ratesContainer != null && ratesContainer.getBase().equals(fromCurrencyCode)) {
            return ratesContainer.getRate(toCurrencyCode);
        }
        return null;
    }

    private void initToViewPager() {
        toAdapter = new CurrencyPagerAdapter(new InputChangedListener() {
            @Override
            public void onInputChanged() {
                onToValueChanged();
            }

            @Override
            public void onFocus() {
                mode = Mode.BOTTOM_TO_TOP;
                calculateExchange();
            }
        }, false);
        toPager.setAdapter(new InfinitePagerAdapter(toAdapter));
        toIndicator.setViewPager(toPager);
        toPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                int pos = position % accounts.getAccounts().size(); // for infinite pager
                toAccount = accounts.getAccounts().get(pos);
                clearAmountAndRate(toAdapter, toPager, toAccount.getCurrencyCode());
                updateHeaderRate();
                calculateExchange();
            }
        });
        toAccount = accounts.getAccounts().get(1);
        toPager.setCurrentItem(1);
    }

    private void onToValueChanged() {
        calculateExchange();
    }

    private void updateHeaderRate() {
        if (fromAccount.getCurrencyCode().equals(toAccount.getCurrencyCode())) {
            conversionRateTextView.setVisibility(View.GONE);
        } else {
            conversionRateTextView.setVisibility(View.VISIBLE);
            String fromCurrencyCode = fromAccount.getCurrencyCode();
            String toCurrencyCode = toAccount.getCurrencyCode();
            conversionRateTextView.setText(StringUtils.getRateString(this, fromCurrencyCode, toCurrencyCode, getRate(), "       "));
        }
    }

    private void validate() {
        if (fromAccount == null) {
            return;
        }
        double fromInput = getFromInput();
        boolean hasFunds = fromAccount.getAmount() >= fromInput;
        boolean enable = fromInput > 0 && getToInput() > 0 && getRate() != null && hasFunds;
        fromAdapter.highlightFunds(fromPager, fromAccount.getCurrencyCode(), hasFunds);
        tickButton.post(() -> {
            tickButton.setClickable(enable);
            tickButton.setImageAlpha(enable ? 255 : 100);
        });

    }

    private double getFromInput() {
        if (fromAccount != null) {
            String fromCurrencyCode = fromAccount.getCurrencyCode();
            return fromAdapter.getInput(fromPager, fromCurrencyCode);
        }
        return 0;
    }

    private double getToInput() {
        if (toAccount != null) {
            String toCurrencyCode = toAccount.getCurrencyCode();
            return toAdapter.getInput(toPager, toCurrencyCode);
        }
        return 0;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
