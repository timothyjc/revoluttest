package timclark.revoluttest.view.adapters;

import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Stack;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timclark.revoluttest.R;
import timclark.revoluttest.RevolutApplication;
import timclark.revoluttest.model.Account;
import timclark.revoluttest.model.Accounts;

public class CurrencyPagerAdapter extends PagerAdapter {

    @Inject Accounts accounts;

    private Stack<View> recycledViews = new Stack<>();

    private InputChangedListener listener;
    private boolean isFromPanel;

    public CurrencyPagerAdapter(InputChangedListener listener, boolean isFromPanel) {
        RevolutApplication.getAppComponent().inject(this);
        this.listener = listener;
        this.isFromPanel = isFromPanel;
    }

    @Override
    public int getCount() {
        return accounts.getAccounts().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // TODO: Maybe better to have used a FragmentStatePagerAdapter
        // https://stackoverflow.com/a/19020687/396106
        View view;
        ViewHolder holder;
        if (recycledViews.isEmpty()) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.exchange_panel, null);
            holder = new ViewHolder(view);
            view.setTag(R.id.panel_view_holder_id, holder);

            holder.inputEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    Observable.just(charSequence)
                            .observeOn(Schedulers.computation())
                            .subscribeOn(Schedulers.computation())
                            .map(CharSequence::toString)
                            .map(s -> TextUtils.isEmpty(s) ? 0 : Double.parseDouble(s))
                            .doOnNext(d -> updatePrefix(holder.inputPrefixTextView, d))
                            .subscribe(d -> listener.onInputChanged());

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            holder.inputEditText.setOnFocusChangeListener((v, b) -> {
                if (v.hasFocus()) {
                    holder.rateTextView.setText("");
                    listener.onFocus();
                }
            });


        } else {
            view = recycledViews.pop();
            holder = (ViewHolder) view.getTag(R.id.panel_view_holder_id);
        }

        Account account = accounts.getAccounts().get(position);
        String currencyCode = account.getCurrencyCode();
        holder.currencyCodeTextView.setText(currencyCode);
        NumberFormat amountFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        amountFormat.setCurrency(Currency.getInstance(currencyCode));
        holder.balanceTextView.setText(Html.fromHtml(String.format(container.getContext().getString(R.string.you_have), amountFormat.format(account.getAmount()))));

        container.addView(view);
        view.setTag(currencyCode);
        return view;
    }

    private void updatePrefix(TextView prefixTextView, double amount) {
        prefixTextView.post(() -> {
            if (amount == 0) {
                prefixTextView.setText("");
                prefixTextView.setVisibility(View.GONE);
            } else if (isFromPanel) {
                prefixTextView.setText("-");
                prefixTextView.setVisibility(View.VISIBLE);
            } else {
                prefixTextView.setText("+");
                prefixTextView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
        recycledViews.push(view);
    }

    public void updatePanel(View container, String currencyCode, double amount, String rateString) {
        View view = container.findViewWithTag(currencyCode);
        if (view != null) {
            ViewHolder holder = (ViewHolder) view.getTag(R.id.panel_view_holder_id);
            String newInputAmount;
            if (amount == 0) {
                newInputAmount = "";
            } else {
                newInputAmount = String.format(Locale.getDefault(), "%.2f", amount);
            }
            if (!holder.inputEditText.getText().toString().equals(newInputAmount)) {
                holder.inputEditText.setText(newInputAmount);
            }
            holder.rateTextView.setText(rateString);
        }
    }

    public double getInput(View container, String currencyCode) {
        View view = container.findViewWithTag(currencyCode);
        if (view != null) {
            ViewHolder holder = (ViewHolder) view.getTag(R.id.panel_view_holder_id);
            String text = holder.inputEditText.getText().toString();
            if (!TextUtils.isEmpty(text)) {
                return Double.parseDouble(text);
            }
        }
        return 0;
    }

    public void highlightFunds(View container, String currencyCode, boolean hasFunds) {
        View view = container.findViewWithTag(currencyCode);
        if (view != null) {
            ViewHolder holder = (ViewHolder) view.getTag(R.id.panel_view_holder_id);
            holder.balanceTextView.post(() -> {
                if (hasFunds) {
                    holder.balanceTextView.setBackground(null);
                } else {
                    holder.balanceTextView.setBackgroundResource(R.drawable.error_background);
                }
            });

        }
    }

    public void onDestroy() {
        recycledViews.clear();
    }

    static class ViewHolder {
        @BindView(R.id.balance) TextView balanceTextView;
        @BindView(R.id.currency_code) TextView currencyCodeTextView;
        @BindView(R.id.input) EditText inputEditText;
        @BindView(R.id.input_prefix) TextView inputPrefixTextView;
        @BindView(R.id.rate) TextView rateTextView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface InputChangedListener {
        void onInputChanged();

        void onFocus();
    }

}