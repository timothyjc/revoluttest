package timclark.revoluttest.api;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import timclark.revoluttest.model.RatesContainer;

public class FixerService {

    private static final String FIXER_URL = "http://api.fixer.io/";

    public FixerApi getApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FIXER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(FixerApi.class);
    }

    public interface FixerApi {

        @GET("latest?symbols=EUR,GBP,USD")
        Call<RatesContainer> listRates(@Query("base") String currencyCode);


    }

}
