package timclark.revoluttest.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

import timclark.revoluttest.RevolutApplication;
import timclark.revoluttest.model.RatesContainer;
import timclark.revoluttest.repository.RatesRepository;

public class ExchangeViewModel extends ViewModel {

    @Inject RatesRepository ratesRepository;

    private LiveData<RatesContainer> rates;

    public ExchangeViewModel() {
        RevolutApplication.getAppComponent().inject(this);
        rates = ratesRepository.getRates();
    }

    public LiveData<RatesContainer> getRates() {
        return rates;
    }

    public void refreshRates(String currencyCode) {
        ratesRepository.refreshRates(currencyCode);
    }

}
