package timclark.revoluttest.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timclark.revoluttest.model.Account;
import timclark.revoluttest.model.Accounts;

@Module
public class AccountsModule {

    @Provides
    @Singleton
    Accounts provideAccounts() {
        // dummy accounts container for now
        Accounts accounts = new Accounts();
        accounts.add(new Account("EUR", 40));
        accounts.add(new Account("GBP", 50));
        accounts.add(new Account("USD", 60));
        return accounts;
    }

}
