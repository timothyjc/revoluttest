package timclark.revoluttest.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timclark.revoluttest.api.FixerService;
import timclark.revoluttest.api.FixerService.FixerApi;

@Module
public class ApiModule {

    @Provides
    @Singleton
    FixerApi provideFixerApi() {
        return new FixerService().getApi();
    }


}
