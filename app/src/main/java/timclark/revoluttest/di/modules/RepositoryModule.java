package timclark.revoluttest.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timclark.revoluttest.repository.RatesCache;
import timclark.revoluttest.repository.RatesRepository;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    RatesRepository provideRatesRepository() {
        return new RatesRepository();
    }

    @Provides
    @Singleton
    RatesCache provideRatesCache() {
        return new RatesCache();
    }
}
