package timclark.revoluttest.di.components;


import javax.inject.Singleton;

import dagger.Component;
import timclark.revoluttest.di.modules.AccountsModule;
import timclark.revoluttest.di.modules.ApiModule;
import timclark.revoluttest.di.modules.AppModule;
import timclark.revoluttest.di.modules.RepositoryModule;
import timclark.revoluttest.repository.RatesRepository;
import timclark.revoluttest.view.ExchangeActivity;
import timclark.revoluttest.view.adapters.CurrencyPagerAdapter;
import timclark.revoluttest.viewmodel.ExchangeViewModel;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class, RepositoryModule.class, AccountsModule.class})
public interface AppComponent {

    void inject(ExchangeActivity exchangeActivity);

    void inject(RatesRepository ratesRepository);

    void inject(ExchangeViewModel exchangeViewModel);

    void inject(CurrencyPagerAdapter currencyPagerAdapter);

}
