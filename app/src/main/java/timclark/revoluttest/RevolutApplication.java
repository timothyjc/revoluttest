package timclark.revoluttest;

import android.app.Application;
import android.os.StrictMode;

import timber.log.Timber;
import timclark.revoluttest.di.components.AppComponent;
import timclark.revoluttest.di.components.DaggerAppComponent;
import timclark.revoluttest.di.modules.AccountsModule;
import timclark.revoluttest.di.modules.ApiModule;
import timclark.revoluttest.di.modules.AppModule;
import timclark.revoluttest.di.modules.RepositoryModule;

public class RevolutApplication extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }

        super.onCreate();

//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return;
//        }
//        LeakCanary.install(this);

        // TODO: A better way of injecting... https://medium.com/@iammert/new-android-injector-with-dagger-2-part-1-8baa60152abe
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule())
                .accountsModule(new AccountsModule())
                .repositoryModule(new RepositoryModule())
                .build();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

}
