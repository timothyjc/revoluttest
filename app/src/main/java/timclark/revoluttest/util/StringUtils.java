package timclark.revoluttest.util;

import android.content.Context;
import android.support.annotation.Nullable;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import timclark.revoluttest.R;

public class StringUtils {

    public static String getRateString(Context context, String firstCurrencyCode, String secondCurrencyCode, @Nullable Double rate, String noRateString) {

        NumberFormat firstFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        firstFormat.setCurrency(Currency.getInstance(firstCurrencyCode));
        firstFormat.setMinimumFractionDigits(0);
        firstFormat.setMaximumFractionDigits(0);
        String firstAmount = firstFormat.format(1);

        NumberFormat secondFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        secondFormat.setCurrency(Currency.getInstance(secondCurrencyCode));
        secondFormat.setMinimumFractionDigits(4);
        secondFormat.setMaximumFractionDigits(4);
        String secondAmount = noRateString;
        if (rate != null) {
            secondAmount = secondFormat.format(rate);
        }

        return String.format(context.getString(R.string.rate_string), firstAmount, secondAmount);

    }

}
